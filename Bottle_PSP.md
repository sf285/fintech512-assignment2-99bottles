# PSP for Bottles Song
#Problem Description

Write code to pass the tests for 99 bottles

1. Fork this repo.
2. Ensure that you can build the code and run the tests. The repo has a single passing test.
3. Once you can build and run the code, set a two hour timer, and continue with the following steps.
4. Remove the '@Disabled("Remove this line to add this test.")' line from the next test in BottlesTest.Java
5. Write the code to pass the test enabled in step 4.
6. When the code passess all enabled tests, commit your changes. The commit message should name the test that was passed.
7. Return to step 4, until you pass all tests, or two hours have passed.

#Design 
|   Number    | 50 lines of code |
|:-----------:|:----------------:|
| VerseNumber | 40 line of code  |
|    Song     | 10 lines of code |


#Planning 
|   Total   | 90 mins |
|:---------:|:-------:|
|   Verse   | 60 mins |
|   Song    | 30 mins |

## Development

| Issues |      |
| :----: | ---- |
|        |      |
|        |      |
|        |      |
|        |      |

|   Development Time    |         |
|:---------------------:|---------|
|  testAnotherVerse()   | 20 mins |
|     testVerse2()      | 5 mins  |
|     testVerse1()      | 3 mins  |
|     testVerse0()      | 3 mins  |
|  testACoupleVerses()  | 20 mins |
|   testAFewVerses()    | 3 mins  |
| testTheWholeSong()    |  5 min  |

## Testing

|              Defect              | Testing Time |
|:--------------------------------:|--------------|
|   testAnotherVerse() No defect   | 1 min        |
|     testVerse2()   No defect     | 1 min        |
|     testVerse1()   No defect     | 1 min        |
|     testVerse0()   No defect     | 1 min        |
|  testACoupleVerses() No defect   | 1 min        |
|   testAFewVerses()  No defect    | 1 min        |
| testTheWholeSong()  No defect    | 1 min        |
## Evaluation

- Total time: 59
- Notable issues: A Little hard for me to change my language from c/c++ to java
